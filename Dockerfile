FROM node:18-alpine AS builder

WORKDIR /app

RUN apk update
RUN apk add git

# install deps
COPY package.json .
COPY package-lock.json .
RUN npm install

# copy app
COPY src/ /app/src
COPY tsconfig.json /app/tsconfig.json

# Build prod app
RUN npm run build

# Build the real container
FROM node:18-alpine

WORKDIR /app

COPY --from=builder /app/dist .

# install deps
COPY package.json .
COPY package-lock.json .
RUN npm install --omit=dev

COPY schema.graphql .

CMD ["node", "-r", "dotenv/config", "./index.js"]
