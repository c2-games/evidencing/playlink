# PlayLink

PlayLink is the API for the cyber-atlas and related technologies. Up your game
by connecting your event with players and other events!

## Getting started

Under construction, feel free to play around though!

### Setup

Copy and modify the following:

- `sample.env.playlink` to `.env.playlink`
- `sample.env.neo4j` to `.env.neo4j`

A `docker-compose.yml` file has been provided for quick setup.

```sh
docker-compose up
```

If things look like they worked, then you should be able to go to:

- `http://localhost:7474/browser/` (neo4j)
- `http://localhost:4000/` (GraphQL)

The `neo4j` directory holds all the data mapped into the docker container for the neo4j instance. Things should just work,
but the `neo4j` container drops permissions to the `neo4j` user, so some edits to mapped directories may need to
be changed in the container so they are properly reflected on your system.

#### Importing and Exporting data

Sample data is provided in `neo4j/import` directory and can be imported or exported using the `apoc` extension.

##### Importing data

This repository exports to `cypher`, so this can be directly executed on a database. To execute it against this database,
use the `cypher-shell` built into the neo4j container.

```sh
cat neo4j/import/all.cypher | docker-compose exec -T neo4j cypher-shell -a neo4j://neo4j:7687 -u neo4j -p NeedsALongPassword --format verbose
```

For more information, reference the docs [here](https://neo4j.com/docs/apoc/current/import/)

##### Exporting data

Exported data will end up in the `neo4j/import` directory. If you get a permission denied error, you likely need to exec
into the neo4j container and change the ownership of `/import`.

```sh
docker-compose exec neo4j chown -R neo4j:neo4j /import
```

```cypher
CALL apoc.export.cypher.all("all.cypher", {
    format: "cypher-shell",
    useOptimizations: {type: "UNWIND_BATCH", unwindBatchSize: 20}
})
```

For more information, reference the docs [here](https://neo4j.com/docs/apoc/current/export/)

## Reset the database

This command will match every node in your database, delete any relationships, then delete the node itself.

```cypher
MATCH (n) DETACH DELETE n
```

## Development

Use the `generated.graphql` schema file to build an application targeting this API.
