import { readFileSync, writeFileSync } from 'fs';
import { Neo4jGraphQL } from '@neo4j/graphql';
import { createYoga } from 'graphql-yoga';
import { createServer } from 'http';
import neo4j from 'neo4j-driver';
import * as dotenv from 'dotenv';
import { printSchema } from 'graphql';

dotenv.config();

// Create Neo4j driver instance
const driver = neo4j.driver(process.env.DB_URI, neo4j.auth.basic(process.env.DB_USER, process.env.DB_PASSWORD));

// we must convert the file Buffer to a UTF-8 string
const typeDefs = readFileSync('./schema.graphql', 'utf-8');

// Create instance that contains executable GraphQL schema from GraphQL type definitions
const schema = await new Neo4jGraphQL({
  typeDefs,
  driver,
  config: {
    driverConfig: {
      database: process.env.DB_NAME
    }
  }
}).getSchema();

const yoga = createYoga({ schema });
const server = createServer(yoga);

server.listen(4000, () => {
  const details = server.address() as { port: number; family: string; address: string };
  console.log(
    `🚀  Server ready at: ${details.family} http://${details.address}:${details.port}${yoga.graphqlEndpoint}`
  );
  writeFileSync('./generated.graphql', printSchema(schema));
  console.log('Generated schema written to ./generate.graphql');
});
